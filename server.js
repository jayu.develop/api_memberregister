const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const smartCard = require('./routes/api/readCard')
const visitorCard = require('./routes/api/visitor_card')
const memberRegister = require('./routes/api/memberRegister')
const port = 3100

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Mysql API' })
})

//Smart Card reader.
app.get('/readCard', smartCard.readCard)
app.delete('/clearCard', smartCard.clearCard)
//Member
app.get('/memberRegister', memberRegister.getMemberById)
app.put('/updateMember', memberRegister.updateMemberById)
app.post('/insertMember', memberRegister.insertMember)
//Visitor Card
app.get('/visitorCard', visitorCard.getVisitorCardAll)
app.get('/visitorCardById', visitorCard.getVisitorCardById)
app.put('/resetCard/:id', visitorCard.resetVisitorCardById)
app.get('/updateVisitorCard', visitorCard.updateVisitorCardById)

app.listen(port, () => {
 console.log(`App running on port ${port}.`)
})
