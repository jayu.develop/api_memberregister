const { con } = require('../../config/config')
const SHA256 = require('js-sha256')

const getMemberById = (request, response) => {
  const { cardId, fnameEn, lnameEn } = request.query
  console.log(cardId + "|" + fnameEn + "|" + lnameEn)

  con.query(`SELECT * FROM ${process.env.DATABASE_DB1}.member_register WHERE regis_card_id = ? AND regis_fname_en = ? AND regis_lname_en = ?`, [cardId, fnameEn, lnameEn], (error, results) => {
    if(error) throw error

    try {
    results.length == 0 ? response.status(200).json({ error: true, msg: "Data not found." }) : true

    temparrays = new Array()
    // console.log(results)
    results.forEach(function (result, index) {
      data = {
        "error" : false,
        "regis_card_id" : result.regis_card_id,
        "regis_barcode" : result.regis_barcode,
        "regis_uid" : result.regis_uid,
        "regis_prefix_en" : result.regis_prefix_en,
        "regis_patron_fname_en" : result.regis_fname_en,
        "regis_patron_lname_en" : result.regis_lname_en,
        "regis_prefix_th" : result.regis_prefix_th,
        "regis_patron_fname_th" : result.regis_fname_th,
        "regis_patron_lname_th" : result.regis_lname_th,
        "regis_type" : result.regis_type,
        "regis_total" : result.regis_total,
        "regis_birthday" : result.regis_birthday,
        "regis_sex" : result.regis_sex,
        "regis_issue" : result.regis_issue,
        "regis_expire" : result.regis_expire,
        "regis_address" : result.regis_address,
        "regis_status" : result.regis_status,
        "regis_tel" : result.regis_tel,
        "regis_email" : result.regis_email
      }
      temparrays.push(data)
    })

    // console.log(temparrays)
    response.status(200).json(temparrays)
    }
    catch(error) { response.json(error) }
  })
}

const updateMemberById = (request, response) => {
  const { 
    cardId, barCode, Uid, prefixEn, fnameEn, lnameEn, prefixTh, 
    fnameTh, lnameTh, type, birthday, sex, issue, expire, address 
  } = request.query

  console.log(cardId + "|" + prefixEn + "|" + fnameEn + "|" + lnameEn)

  con.query(`UPDATE ${process.env.DATABASE_DB1}.member_register SET regis_barcode = ?, regis_uid = ?, regis_prefix_en = ?, 
  regis_fname_en = ?, regis_lname_en = ?, regis_prefix_th = ?, regis_fname_th = ?, regis_lname_th = ?, regis_type = ?, 
  regis_total = ?, regis_birthday = ?, regis_sex = ?, regis_issue = ?, regis_expire = ?, regis_address = ? 
  WHERE regis_card_id = ?`, 
  [barCode, Uid, prefixEn, fnameEn, lnameEn, prefixTh, fnameTh, lnameTh, type, 1, birthday, sex, issue, expire, address, cardId], (error, results) => {

    if (error) throw error
    try {
      results.affectedRows == 0 ? 
      response.status(404).json({ error: true, msg : "Member not found." }) : response.status(200).json({ error: false, msg : "Update success." })
    }
    catch(error) { response.json(error) }
  })
}

const insertMember = (request, response) => {
  const { 
    cardId, barCode, Uid, prefixEn, fnameEn, lnameEn, prefixTh, 
    fnameTh, lnameTh, type, birthday, sex, issue, expire, address, tel, email, remark 
  } = request.query

  const funcCardId = SHA256(cardId)
  const unix = Math.round(+new Date()/1000)
  const funcImgCamera = funcCardId+"|"+new Date().toLocaleString()
  const funcImgCard = funcCardId+".jpg"
  const funcStatus = "M"

  console.log(cardId + "|" + prefixEn + "|" + fnameEn + "|" + lnameEn)

  const resRegis = con.query(`INSERT INTO ${process.env.DATABASE_DB1}.member_register (regis_card_id, regis_barcode, regis_uid, regis_prefix_en, 
  regis_fname_en, regis_lname_en, regis_prefix_th, regis_fname_th, regis_lname_th, regis_type, regis_total, regis_img_camera, 
  regis_img_card, regis_birthday, regis_sex, regis_issue, regis_expire, regis_address, regis_status, regis_tel, regis_email, regis_remark) 
  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
  [funcCardId, barCode, Uid, prefixEn, fnameEn, lnameEn, prefixTh, fnameTh, lnameTh, type, 1, funcImgCamera, funcImgCard ,birthday, 
    sex, issue, expire, address, funcStatus, tel, email, remark], (error, results) => {
    
    if (error) throw error
    try {
      // results.affectedRows == 0 ? 
      // response.status(404).json({ error: true, msg : "Register member false." }) : response.status(200).json({ error: false, msg : "Register success." })
      results.affectedRows == 0 ? console.log("Register false.") : console.log("Register success.")
    }
    catch(error) { response.json(error) }
  })

  if(resRegis) {
    con.query(`UPDATE ${process.env.DATABASE_DB1}.visitor_card SET visitorId = ? WHERE cardUID = ?`, [funcCardId, barCode], (error, results) => {
      if (error) throw error
      try {
        results.affectedRows == 0 ? console.log("Update visitor card false.") : console.log("Update visitor card success.")
      }
      catch(error) { response.json(error) }
    })
  }   
  
}


module.exports = {
  getMemberById,
  updateMemberById,
  insertMember
}