const { con } = require('../../config/config')
const SHA256 = require('js-sha256')

generateSex = (data) => {
  if(data == 1 || data == 2) {
    data == 1 ? data = "M" : data = "F"
    return data
  }else if(data == "M" || data == "MAN" || data == "MALE") {
    data = "M"
    return data
  }else {
    data = "F"
    return data
  }
}

//Read card insert
const readCard = (request, response) => {
  con.query(`SELECT * FROM ${process.env.DATABASE_DB1}.member_reader ORDER BY reader_create_at DESC LIMIT 1`, (err, results, fields) => {
    // console.log(results[0].reader_card_id)
    if(err) throw err
    results.length == 0 ? response.status(200).json({ error: true, msg: "Data Empty." }) : true

    temparrays = new Array()
    results.forEach(function (result, index) {
      data = {
        "error" : false,
        "reader_card_id" : result.reader_card_id,
        "reader_card_id_hash" : SHA256(result.reader_card_id),
        "reader_uid" : result.reader_uid,
        "reader_passport" : result.reader_passport,
        "reader_prefix_en" : result.reader_prefix_en,
        "reader_patron_fname_en" : result.reader_patron_fname_en.toUpperCase(),
        "reader_patron_lname_en" : result.reader_patron_lname_en.toUpperCase(),
        "reader_prefix_th" : result.reader_prefix_th,
        "reader_patron_fname_th" : result.reader_patron_fname_th,
        "reader_patron_lname_th" : result.reader_patron_lname_th,
        "reader_birthday" : result.reader_birthday,
        "reader_sex" : generateSex(result.reader_sex),
        "reader_issue" : result.reader_issue,
        "reader_expire" : result.reader_expire,
        "reader_img_card" : result.reader_img_card,
        "reader_create_at" : new Date(result.reader_create_at).toLocaleString(),
        "reader_address" : result.reader_address.replace(/\s/g, ''),
        "reader_status" : result.reader_status
      }
      temparrays.push(data)
    })

    // console.log(temparrays)
    response.status(200).json(temparrays)
  })
}

const clearCard = (request, response) => {
  con.query(`DELETE FROM ${process.env.DATABASE_DB1}.member_reader`, (error, results) => {
    if (error) throw error
    
    response.status(200).json('Delete success.')
  })
}

module.exports = {
  readCard,
  clearCard
}