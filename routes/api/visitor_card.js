const { con } = require('../../config/config')
const axios = require('axios')

const getVisitorCardAll = (request, response) => {
  con.query(`SELECT * FROM ${process.env.DATABASE_DB1}.visitor_card`, (error, results) => {
    if(error) throw error

    try {
      results.length == 0 ?
      response.status(404).json({ error: true, msg: "Data Empty" }) : response.status(200).json(results)
    }
    catch(error) {response.json(error)}
  })
}

const getVisitorCardById = (request, response) => {
  const { cardId, cardUID } = request.query
  console.log("Req: "+cardId + "|" + cardUID)

  con.query(`SELECT * FROM ${process.env.DATABASE_DB1}.visitor_card WHERE cardUID = ? || cardId = ?`, [cardUID, cardId], (error, results) => {
    if(error) throw error

    try {
      results.length == 0 ?
      response.status(404).json({ error: true, msg: "Visitor card not found." }) : response.status(200).json(results)
    }
    catch(error) {response.json(error)}
  })
}

const resetVisitorCardById = (request, response) => {
  const id = parseInt(request.params.id)
  console.log("Req: "+id)
  
  con.query(`UPDATE ${process.env.DATABASE_DB1}.visitor_card SET visitorId = NULL WHERE cardId = ?`, [id], (error, results) => {
    if(error) throw error 

    try {
      results.affectedRows == 0 ?
      response.status(404).json({ error: true, msg: "Visitor card not found." }) : response.status(200).json({ error: false, msg: "Update success." })
    }
    catch(error) { response.json(error) }
  })
}

const updateVisitorCardById = (request, response) => {
  const { cardId, cardName, cardUID } = request.query
  console.log("Req: "+cardId +"|"+ cardName +"|"+ cardUID)
  
  axios.get(`${process.env.HOST_CALL_API}/visitorCardById/?cardId=${cardId}`).then(res => {
    // response.status(200).json(res.data[0].visitorId)
    if(res.data[0].visitorId === null) {

      con.query(`SELECT cardUID FROM ${process.env.DATABASE_DB1}.visitor_card`, (error, results) => {
        if(error) throw error
        var items = results.find(result => result.cardUID === cardUID)
        console.log(items)

        if(items == undefined) {
          con.query(`UPDATE ${process.env.DATABASE_DB1}.visitor_card SET cardName = ?, cardUID = ? WHERE cardId = ?`, [cardName, cardUID, cardId], (error, results) => {
            if(error) throw error
    
            try {
              results.affectedRows == 0 ?
              response.status(404).json({ error: true, msg: "Update false." }) : response.status(200).json({ error: false, msg: "Update success." })
            }
            catch(error) { response.json(error) }
          })          
        }else {
          response.status(404).json({ error: true, msg: "UID or Barcode duplicate" })          
        }
      })

    }else {
      response.status(200).json({ error: true, msg: "Can't update This card is in use." })  
    }

  })
  .catch(error => {
    return response.status(404).json({ error: true, msg: "Visitor by id card not found." })
  })
}

module.exports = {
  getVisitorCardAll,
  getVisitorCardById,
  resetVisitorCardById,
  updateVisitorCardById
}