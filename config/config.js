const dotenv = require('dotenv');
dotenv.config();

const mysql = require('mysql')
const con = mysql.createConnection({
  host: process.env.HOST_API,
  database: process.env.DATABASE_DB1,
  user: process.env.USERNAME_API,
  password: process.env.PASSWORD_API,
  port: process.env.PORT_DB,
  multipleStatements: true
})

con.connect(function(error) {
  if (error) throw error;
  console.log("Connected")
})

module.exports = { con };